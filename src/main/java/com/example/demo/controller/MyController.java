package com.example.demo.controller;

import com.example.demo.exceptions.BookNotFoundException;
import com.example.demo.exceptions.BookUnSupportedFieldPatchException;
import com.example.demo.models.Book;
import com.example.demo.models.Student;
import com.example.demo.models.User;
import com.example.demo.repositories.BookRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.service.BookManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MyController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    StudentRepository studentRepository;

    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        return a + b;
    }

    @GetMapping("/tinhgiaithua")
    Integer tinhGiaiThua(@RequestParam("a")Integer a){
        int b=Math.abs(a);
        int rs=1;
        for (int i=1;i<=b;i++){
            rs=i*rs;
        }
        return rs;
    }
    @PostMapping("/save-book")
    String saveBook(@RequestBody Book book){
        return book.getName();
    }
    @PostMapping("/add-user")
    void addUser(@RequestBody User user){
        userRepository.save(user);
    }

    @PostMapping("/add-student")
    String addStudent(@RequestBody Student student){
        studentRepository.save(student);
        return student.getName();
    }


}
